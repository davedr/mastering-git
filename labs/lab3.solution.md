# Lab 3

## Solution

#### Step 1: Fork this repo

1. Click the "Fork" button in the top right corner of the repository page.
2. Select your user or organization account to create a fork of the repository.

#### Step 2: Clone your fork and set up origin and upstream remotes properly

4. Clone your fork to your local machine:

```bash
git clone TBD
```

5. Change into the cloned directory:

```bash
cd TBD
```

6. Add the original repository as the "upstream" remote:

```bash
git remote add upstream TBD
```

Now, your fork has two remotes: "origin" (your fork) and "upstream" (the original repository).

#### Step 3: Contribute using a merge request

7. Create a new branch for your contribution:

```bash
git checkout -b <do-not-name-this-my-contribution>
```

#### Step 4: Open autumn2023.md and write down what’s the most valuable thing you learnt so far on this course

9. Write down the most valuable thing you have learned in the course:

```
The most valuable thing I have learned so far in this course is the importance of using version control systems like Git for collaborative software development. Git has allowed me to work on projects more efficiently, collaborate with others, and track changes in the codebase effectively. I now feel more confident in managing projects with Git and using branches, merges, and pull requests to collaborate with other developers.
```

#### Step 5: Commit and push to your fork

10. Stage the changes and commit:

```bash
git add autumn2023.md
git commit -m "Added my most valuable lesson"
```

11. Push the changes to your fork:

```bash
git push origin
```

#### Step 6: Open the merge request

TBD


#### Step 7: CI needs to pass

17. Wait for the Continuous Integration (CI) checks to run. The CI system will
    check whether your changes pass all the necessary tests and meet the
    project's requirements.

18. If the CI checks pass successfully, your pull request will be ready for
    review and merge.


## Conclusion
Congratulations! You have successfully completed the workshop class by forking
a repository, contributing to it via a merge request, and ensuring that the CI
checks pass before merging the changes into the main repository. This workflow
is commonly used in open-source software development projects to encourage
contributions and maintain code quality.
