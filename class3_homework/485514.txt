I enjoy learning a bit about the internals of Git, like that branches and tags are basically pointers to commits.
I also found it intriguing, that it is possible to setup Git config in such a way, that different configuration is used based on the location of the repository in my filesystem. Might use that later.
I don't think I have used anything new yet, but I am looking forward to best practises discussion.
